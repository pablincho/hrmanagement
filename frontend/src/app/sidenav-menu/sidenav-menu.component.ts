import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry } from "@angular/material/icon";

@Component({
  selector: 'sidenav-menu',
  templateUrl: './sidenav-menu.component.html',
  styleUrls: ['./sidenav-menu.component.css']
})
export class SidenavMenuComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      "home-icon",
      sanitizer.bypassSecurityTrustResourceUrl("assets/img/home_icon.svg")
    );
    iconRegistry.addSvgIcon(
      "google-drive-icon",
      sanitizer.bypassSecurityTrustResourceUrl(
        "assets/img/Google_Drive_Logo.svg"
      )
    );
    iconRegistry.addSvgIcon(
      "one-drive-icon",
      sanitizer.bypassSecurityTrustResourceUrl(
        "assets/img/onedrive.svg"
      )
    );
  }

  ngOnInit() {
  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  } 

}

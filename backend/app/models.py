from backend.app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from .permissions import Permission


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    phone = db.Column(db.String(50))
    op_phone = db.Column(db.String(50))
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"))
    server_username = db.Column(db.String(100))
    op_name = db.Column(db.String(50))
    pp_status = db.Column(db.String(100))
    got_refinancing = db.Column(db.Boolean, default=False)

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @property
    def is_admin(self) -> bool:
        """Checks if user is admin"""
        if self.role_id == 3:
            return True
        else:
            return False

    @property
    def is_super(self) -> bool:
        """Checks if user is super"""
        if self.role_id == 2:
            return True
        else:
            return False

    @property
    def is_normal(self) -> bool:
        if self.role_id == 1:
            return True
        else:
            return False

    @staticmethod
    def insert_admin_user(username, password):
        user = db.session.query(User).filter_by(username=username).one_or_none()
        if user is None:
            u = User(username=username, password=password, role_id=3)
            db.session.add(u)
            db.session.commit()

    @staticmethod
    def insert_test_user(username, password):
        """Inserts an test user"""
        user = (
            db.session.query(User).filter_by(username=username).one_or_none()
        )
        if user is None:
            u = User(
                username=username,
                password=password,
                role_id=1,
                phone=3515678901,
                op_phone=3516789012
            )
            db.session.add(u)
            db.session.commit()

    def __repr__(self):
        return "<User> Username -  '{}'".format(self.username)


class Role(db.Model, UserMixin):
    """Model which contains the role of an app user"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship("User", backref="role", lazy="dynamic")

    @staticmethod
    def insert_roles():
        """Fills in roles table with current valid roles"""
        roles = {
            "Usuario": (Permission.USER, True),
            "Super": (Permission.SUPER, False),
            "Administrador": (0xFF, False),
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return "<Role %r>" % self.name

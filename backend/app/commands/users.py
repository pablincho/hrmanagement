import click
from flask import current_app
from flask.cli import with_appcontext
from backend.app.models import Role, User

click.disable_unicode_literals_warning = True


@click.group(name='users')
def cmd():
    """Users management commands"""
    pass


@cmd.command()
@with_appcontext
def insert_roles():
    """Update the additional roles - only used if new roles need to be added to existing db"""
    Role.insert_roles()
    current_app.logger.info("Inserted roles")


@cmd.command()
@click.option("--user", prompt="Enter username", help="Name or DNI.")
@click.option('--password', prompt=True, confirmation_prompt=True, hide_input=True)
@with_appcontext
def insert_admin_user(user: str, password: str):
    """Command line function to insert an admin user"""
    User.insert_admin_user(user, password)
    current_app.logger.info("Inserted admin user")


@cmd.command()
@click.option("--user", prompt="Enter username", help="Name or DNI.")
@click.option('--password', prompt=True, confirmation_prompt=True, hide_input=True)
@with_appcontext
def insert_test_user(user: str, password: str):
    """Command line function to insert a test user"""
    User.insert_test_user(user, password)
    current_app.logger.info("Inserted test user")

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, ValidationError, NumberRange
from flask_wtf.file import FileField, FileRequired, FileAllowed


def ten_characters(form, field):
    if field.data:
        if len(field.data) != 10:
            raise ValidationError('Formato incorrecto')
        try:
            val = int(field.data)
        except ValueError:
            raise ValidationError('Sólo números')


class LoginForm(FlaskForm):
    user = StringField("Usuario", validators=[DataRequired(message='Campo obligatorio')])
    password = PasswordField("Password", validators=[DataRequired(message='Campo obligatorio')])
    remember_me = BooleanField("Recordarme")
    submit = SubmitField("Entrar")
    phone = StringField("Teléfono", validators=[DataRequired(message='Campo obligatorio'), ten_characters])


class FileForm(FlaskForm):
    sheet_file = FileField("Archivo",
                           validators=[FileRequired(),
                                       FileAllowed(['xls', 'xlsx', 'ods', 'csv'],
                                       'Sólo hojas de cálculo permitidas')])
    submit = SubmitField("Subir archivo")


class ContactPhoneForm(FlaskForm):
    phone = StringField("Teléfono", validators=[DataRequired(message='Campo obligatorio')])
    submit = SubmitField("Cambiar")

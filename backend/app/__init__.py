from flask import Flask, render_template
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from flask_wtf.csrf import CSRFProtect
import flask_excel
import os

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)
    login_manager = LoginManager()
    login_manager.login_view = "main.login"
    login_manager.init_app(app)
    CSRFProtect(app)
    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from ..app.commands.users import cmd as users_cmd
    app.cli.add_command(users_cmd)
    flask_excel.init_excel(app)
    db.init_app(app)
    Migrate(app, db)

    # @app.errorhandler(404)
    # def page_not_found(error):
    #     """Renders template for 404 error"""
    #     return render_template("404.html"), 404
    #
    @app.errorhandler(500)
    def internal_error(error):
        """Renders template for 500 error"""
        return render_template('500.html'), 500

    # @app.errorhandler(Exception)
    # def internal_error_1(e):
    #     """Renders template for 500 error"""
    #     return render_template('500.html'), 500

    return app